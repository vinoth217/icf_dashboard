import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { DataTableComponent } from './widget/data-table/data-table.component';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';

import { MaterialModule } from '../../app/material.module'

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule } from "@angular/forms";
import { Ng2CompleterModule } from "ng2-completer";

@NgModule({
  declarations: [
    DataTableComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    RouterModule,    
    Ng2SmartTableModule,
    FormsModule,
    Ng2CompleterModule,
    MaterialModule
  ],
  exports: [
    DataTableComponent,
    HeaderComponent,
    FooterComponent
  ]
})
export class SharedModule { }
