import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { DataTableServiceService } from './../../../http/data-table-service.service'

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit {

  @Input() gridUrl;
  gridList: any;
  settings = {
    delete: {
      confirmDelete: true,

      deleteButtonContent: '<i class="fa fa-trash" style="font-size:32px"></i>',
      saveButtonContent: 'save',
      cancelButtonContent: 'cancel',
      
      
    },
    add: {
      confirmCreate: true,
      addButtonContent:'<i class="fa fa-plus" style="font-size:32px"></i>'
    },
    edit: {
      confirmSave: true,
      editButtonContent:'<i class="fa fa-edit" style="font-size:32px"></i>'
    },
  };
  data = [];

  constructor(private DataTableServiceService: DataTableServiceService) {    
   }

  ngOnInit() {
    this.getDataTable();   
  }
  getDataTable(){
    this.DataTableServiceService.getTableList(this.gridUrl)
      .subscribe(data => {
        this.data = data.items;
        this.settings = data.colum;
      })
  }

 

}
