import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  menu : any = 'menu1';
  isShowDiv = true;
   
  onClick(check){
        if(check==1){
          this.menu = 'menu1';
        }else if(check==2){
          this.menu = 'menu2';
        }else{
          this.menu = 'menu3';
        }    
      
    }

  toggleNotification() {
    this.isShowDiv = !this.isShowDiv;
  }

  constructor() { }

  ngOnInit() {
  }

}
