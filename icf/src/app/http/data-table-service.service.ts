import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataTableServiceService {
  constructor(private http: HttpClient) { }

  getTableList(url): Observable<any> {
    console.log(url)
    return this.http.get(url);
  }
}
