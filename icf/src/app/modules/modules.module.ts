import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InitiativesComponent } from './../modules/initiatives/initiatives.component';
import { DashboardComponent } from './../modules/dashboard/dashboard.component'
import { SharedModule } from './../shared/shared.module';

import { MaterialModule } from '../../app/material.module'

@NgModule({
  declarations: [
    InitiativesComponent,
    DashboardComponent
  ],
  imports: [
    CommonModule,    
    SharedModule,
    MaterialModule
  ]
})
export class ModulesModule { }
